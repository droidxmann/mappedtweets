import os
import simplejson
import urllib
import logging
import cgi

from google.appengine.api import memcache
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

api_key = '8ee2a50541944fb9bcedded5165f09d9'

def escape(unescaped):
    escaped = unescaped.replace('"', '\\"').replace("'", "\\'").replace('\n', ' ')
    return cgi.escape(escaped).encode('ascii', 'xmlcharrefreplace')

def getLatLng(location):
    global api_key
    geo = memcache.get(location)
    if geo is None:
        params = urllib.urlencode(dict(query=location), 'UTF-8')
        url = ('http://geocoding.cloudmade.com/%s/geocoding/v2/find.js?%s') % (api_key, params)
        cm = simplejson.loads(urllib.urlopen(url).read())
        if cm:            
            geo = cm["bounds"][0]            
        else:
            geo = [37.781157, -122.398720]        
        memcache.add(location, geo, 60)
    return geo

def searchTweets(query, lat=str(37.781157), lng=str(-122.398720), rd=str(10000)):
    tweets = []
    params = urllib.urlencode(dict(q=query, rpp=50, lang='en', geocode=('%s,%s,%s'+'mi')%(lat,lng,rd)))
    url = 'http://search.twitter.com/search.json?' + params
    logging.error(url)
    search = urllib.urlopen(url)    
    results = simplejson.loads(search.read())
    for result in results["results"]: # result is a list of dictionaries
        logging.error(result)
        tweet = {}
        location = result["location"]        
        geocoding = getLatLng(location)
        tweet["lat"] = geocoding[0]
        tweet["lng"] = geocoding[1]
        tweet["text"] = escape(result["text"])
        tweets.append(tweet)
    return tweets


class MainPage(webapp.RequestHandler):        
    def get(self):
        tweets = []        
        query = 'your keywords'
        template_values = {
            "tweets" : tweets,
            "query" : query
            }
        path = os.path.join(os.path.dirname(__file__), 'map.html')
        self.response.out.write(template.render(path, template_values))
    
    def post(self):
        query = self.request.get('query')
        tweets = searchTweets(query)        
        template_values = {
            "tweets" : tweets,
            "query" : query
            }
        path = os.path.join(os.path.dirname(__file__), 'map.html')
        self.response.out.write(template.render(path, template_values))


application = webapp.WSGIApplication([('/', MainPage)], debug=True)


def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
